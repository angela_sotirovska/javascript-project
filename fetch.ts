export let getUsers = async (url: string) =>{
    const promise: Promise<Response> = fetch(url);

    try{
        const response: Response = await promise;
        const json = await response.json();
    

        return json;
    }
    catch (error){
        return error;
    }

}