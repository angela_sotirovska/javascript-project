import { getUsers } from "./fetch.js";
import { fillInTable } from "./usersTable.js";
const users = await getUsers("https://jsonplaceholder.typicode.com/users");
const table = document.getElementById("table");
fillInTable(users, table);
