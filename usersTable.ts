export function fillInTable(users, table): void{
    users.forEach(function(elem){
        add(table, elem.name, elem.username, elem.email, elem.address.city, elem.company.name);
    })
}
function add(table, name: string, username: string, email: string, city: string, company: string): void{
    let rowCount: number = table.rows.length;
    let row = table.insertRow(rowCount);
    let cell=row.insertCell(0);
    cell.innerHTML=name;
    let cell1=row.insertCell(1);
    cell1.innerHTML=username;
    let cell2=row.insertCell(2);
    cell2.innerHTML=email;
    let cell3=row.insertCell(3);
    cell3.innerHTML=city;
    let cell4=row.insertCell(4);
    cell4.innerHTML=company;
}