export let getUsers = async (url) => {
    const promise = fetch(url);
    try {
        const response = await promise;
        const json = await response.json();
        return json;
    }
    catch (error) {
        return error;
    }
};
